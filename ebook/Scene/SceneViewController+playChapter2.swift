import UIKit

extension SceneViewController {
	
	func playChapter2(_ scene: Chapter.SceneName) {
		switch scene {
		case .part1:
			PlayerSound.instance.resetCurrentTime()
			PlayerSound.instance.playSound(.เพลงหน้า7)
			imageView
				.setupRx()
				.setupGifImage("เข้าลูปหน้า7")
				.setupGifImage("ลูปหน้า7")
				.play(disposeBag)
		case .part2:
			PlayerSound.instance.playSound(.เพลงหน้า7)
			imageView
				.setupRx()
				.setupGifImage("เข้าลูปหน้า8")
				.setupGifImage("ลูปหน้า8")
				.play(disposeBag)
			
			fiveAreaButton.rx.tap
				.bind { [weak self] _ in
					self?.popup(name: "popupหน้า8-1")
				}
				.disposed(by: disposeBag)

			sixAreaButton.rx.tap
				.bind { [weak self] _ in
					self?.popup(name: "popupหน้า8-2")
				}
				.disposed(by: disposeBag)

		case .part3:
			PlayerSound.instance.playSound(.เพลงหน้า7)
			imageView
				.setupRx()
				.setupGifImage("เข้าลูป1หน้า9")
				.setupGifImage("ลูป1หน้า9")
				.play(disposeBag)
			
			oneButton.rx.tap
				.bind { [weak self] _ in
					self?.popup(name: "popupหน้า9")
				}
				.disposed(by: disposeBag)
		case .part4:
			PlayerSound.instance.playSound(.เพลงหน้า7)
			imageView
				.setupRx()
				.setupGifImage("เข้าลูป2หน้า9")
				.setupGifImage("ลูป2หน้า9")
				.play(disposeBag)
			
			oneButton.rx.tap
				.bind { [weak self] _ in
					self?.popup(name: "popupหน้า9")
				}
				.disposed(by: disposeBag)
		case .part5:
			PlayerSound.instance.playSound(.เพลงหน้า10)
			imageView
				.setupRx()
				.setupGifImage("เข้าลูปหน้า10")
				.setupGifImage("ลูปหน้า10")
				.play(disposeBag)
			
			eightAreaButton.rx.tap
				.bind { [weak self] _ in
					self?.popup(name: "popupหน้า10")
				}
				.disposed(by: disposeBag)
			
		case .end:
			closeView()
		default:
			break
		}
	}
}
