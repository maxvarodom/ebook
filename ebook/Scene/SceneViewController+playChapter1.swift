import UIKit

extension SceneViewController {
	
	func playChapter1(_ scene: Chapter.SceneName) {
		switch scene {
		case .part1:
			imageView
				.setupRx()
				.rxGifImage("เข้าลูปหน้า1")
				.flatMap { $0.rxGifImage("ลูปหน้า1") }
				.subscribe()
				.disposed(by: disposeBag)
			PlayerSound.instance.resetCurrentTime()
			PlayerSound.instance.playSound(.moonlight_sonata, numberOfLoops: -1)
			PlayerSound.instance.playSound(.เสียงบรรยาดาศ, numberOfLoops: -1)
		case .part2:
			imageView
				.setupRx()
				.setupGifImage("เข้าลูป1หน้า2")
				.setupGifImage("ลูป1หน้า2")
				.play(disposeBag)
			
			PlayerSound.instance.playSound(.moonlight_sonata, numberOfLoops: -1)
		case .part3:
			imageView
				.setupRx()
				.setupGifImage("เข้าลูป2หน้า2")
				.setupGifImage("ลูป2หน้า2")
				.play(disposeBag)
			
			PlayerSound.instance.playSound(.moonlight_sonata, numberOfLoops: -1)
		case .part4:
			imageView
				.setupRx()
				.setupGifImage("เข้าลูปหน้า3")
				.setupGifImage("ลูปหน้า3")
				.play(disposeBag)
			
			PlayerSound.instance.playSound(.เสียงบรรยากาศหน้า3, numberOfLoops: -1)
			PlayerSound.instance.playSound(.moonlight_sonata, numberOfLoops: -1)

			oneButton.rx.tap
				.bind { [weak self] _ in
					self?.popup(name: "หน้าต่างpopup3")
				}
				.disposed(by: disposeBag)
		case .part5:
			imageView
				.setupRx()
				.setupGifImage("เข้าลูป1หน้า4")
				.setupGifImage("ลูป1หน้า4")
				.play(disposeBag)
			
			PlayerSound.instance.playSound(.เสียงบรรยากาศหน้า4, numberOfLoops: -1)
			PlayerSound.instance.playSound(.moonlight_sonata, numberOfLoops: -1)

			twoButton.rx.tap
				.bind { [weak self] _ in
					self?.popup(name: "popupหน้า4")
				}
				.disposed(by: disposeBag)
		case .part6:
			imageView
				.setupRx()
				.setupGifImage("เข้าลูป2หน้า4")
				.setupGifImage("ลูป2หน้า4")
				.play(disposeBag)
			
			PlayerSound.instance.playSound(.เสียงบรรยากาศหน้า4, numberOfLoops: -1)
			PlayerSound.instance.playSound(.moonlight_sonata, numberOfLoops: -1)

			twoButton.rx.tap
				.bind { [weak self] _ in
					self?.popup(name: "popupหน้า4")
				}
				.disposed(by: disposeBag)
		case .part7:
			PlayerSound.instance.stop()
			imageView
				.setupRx()
				.setupGifImage("เข้าลูป3หน้า4")
				.setupGifImage("ลูป3หน้า4")
				.play(disposeBag)
			
			twoButton.rx.tap
				.bind { [weak self] _ in
					self?.popup(name: "popupหน้า4")
				}
				.disposed(by: disposeBag)
		case .part8:
			imageView
				.setupRx()
				.setupGifImage("เข้าลูป1หน้า5")
				.setupGifImage("ลูป1หน้า5")
				.play(disposeBag)
			
			PlayerSound.instance.playSound(.เสียงบรรยากาศหน้า5_1, numberOfLoops: -1)
			
			oneButton.rx.tap
				.bind { [weak self] _ in
					self?.popup(name: "หน้าต่างpopupหน้า5")
				}
				.disposed(by: disposeBag)
		case .part9:
			imageView
				.setupRx()
				.setupGifImage("เข้าลูป2หน้า5")
				.setupGifImage("ลูป2หน้า5")
				.play(disposeBag)
			PlayerSound.instance.playSound(.เสียงบรรยากาศหน้า5_2, numberOfLoops: -1)
			oneButton.rx.tap
				.bind { [weak self] _ in
					self?.popup(name: "หน้าต่างpopupหน้า5")
				}
				.disposed(by: disposeBag)
		case .part10:
			imageView
				.setupRx()
				.setupGifImage("เข้าลูป1หน้า6")
				.setupGifImage("ลูป1หน้า6")
				.play(disposeBag)
			
			fourAreaButton.rx.tap
				.bind { [weak self] _ in
					self?.popup(name: "popupหน้า6")
				}
				.disposed(by: disposeBag)
			PlayerSound.instance.playSound(.เพลงหน้า6, numberOfLoops: -1)
		case .part11:
			imageView
				.setupRx()
				.setupGifImage("เข้าลูป2หน้า6")
				.setupGifImage("ลูป2หน้า6")
				.play(disposeBag)
			fourAreaButton.rx.tap
				.bind { [weak self] _ in
					self?.popup(name: "popupหน้า6")
				}
				.disposed(by: disposeBag)
			PlayerSound.instance.playSound(.เพลงหน้า6, numberOfLoops: -1)
			
		case .end:
			closeView()
		}
	}
}
