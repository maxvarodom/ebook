import UIKit
import RxSwift
import RxCocoa
import SwiftyGif

class SceneViewController: UIViewController {
	
	@IBOutlet weak var imageView: RxSwiftyGifImageView!
	@IBOutlet weak var oneButton: UIButton!
	@IBOutlet weak var twoButton: UIButton!
	@IBOutlet weak var fourAreaButton: UIButton!
	@IBOutlet weak var fiveAreaButton: UIButton!
	@IBOutlet weak var sixAreaButton: UIButton!
	@IBOutlet weak var eightAreaButton: UIButton!
	@IBOutlet weak var nineAreaButton: UIButton!
	@IBOutlet weak var nextButton: UIButton!
	@IBOutlet weak var backButton: UIButton!

	var popupImageView: PopupImageView?
	
	var gif = SwiftyGifImageView()
	var soundMode: Bool = true
	var scenes: [Chapter.SceneName] = []
	var sceneCounting = Int()
	var chapter: Chapter = .one
	var disposeBag = DisposeBag()
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		playChapter(chapter)
	}
}

extension SceneViewController {
	
	func playChapter(_ chapter: Chapter) {
		scenes = chapter.scenes
		playScene(chapter.scenes.first ?? .part1)
	}
	
	func playScene(_ scene: Chapter.SceneName) {
		disposeBag = DisposeBag()
		imageView.dispose()
		dismissPopup()
		hidenButtonBackFirstChapter()
		switch chapter {
		case .one:
			playChapter1(scene)
		case .two:
			playChapter2(scene)
		case .three:
			playChapter3(scene)
		}
	}
}

extension SceneViewController {
	
	func popup(name: String) {
		PlayerSound.instance.playSound(.เสียงปุ่ม)
		popupImageView = PopupImageView(frame: view.frame)
		guard let `popupImageView` = popupImageView else {
			return
		}
		popupImageView.frame = CGRect(x: 0, y: 0, width: view.frame.width - 400, height: view.frame.height - 400)
		popupImageView.center = CGPoint(x: view.center.x, y: view.center.y)
		popupImageView.setup()
		popupImageView.contentMode = .scaleAspectFit
		popupImageView.image = UIImage(named: name)
		self.view.addSubview(popupImageView)
	}
	
	func dismissPopup() {
		popupImageView?.removeFromSuperview()
		popupImageView = nil
	}
}

extension SceneViewController {
	
	var isNextScene: Bool {
		return (scenes.count > (sceneCounting + 1))
	}
	
	var isBackScene: Bool {
		return (0 != sceneCounting)
	}
	
	func hidenButtonBackFirstChapter() {
		backButton.isHidden = (scenes[sceneCounting] == .part1)
	}
}

extension SceneViewController {
	
	@IBAction func next(sender: UIButton) {
		PlayerSound.instance.playSound(.เสียงปุ่ม)
		guard isNextScene else { return }
		sceneCounting += 1
		PlayerSound.instance.playSound(.เสียงเปลี่ยนหน้า)
		DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
			PlayerSound.instance.stop()
			self.playScene(self.scenes[self.sceneCounting])
		}
	}
	
	@IBAction func back(sender: UIButton) {
		PlayerSound.instance.playSound(.เสียงปุ่ม)
		guard isBackScene else { return }
		sceneCounting -= 1
		PlayerSound.instance.playSound(.เสียงเปลี่ยนหน้า)
		DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
			PlayerSound.instance.stop()
			self.playScene(self.scenes[self.sceneCounting])
		}
	}
	
	@IBAction func sound(sender: UIButton) {
		soundMode = !soundMode
		PlayerSound.instance.playSound(.เสียงปุ่ม)
		if soundMode {
			sender.setImage(UIImage(named: "เปิดเสียง"), for: UIControl.State.normal)
			PlayerSound.instance.setVolume(volume: 1.0)
		} else {
			sender.setImage(UIImage(named: "ปิดเสียง"), for: UIControl.State.normal)
			PlayerSound.instance.setVolume(volume: 0.0)
		}
	}
	
	@IBAction func menu(sender: UIButton) {
		PlayerSound.instance.playSound(.เสียงปุ่ม)
		DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.25) {
			PlayerSound.instance.stop()
			self.closeView()
		}
	}
}

extension SceneViewController {
	
	func closeView() {
		let transition = CATransition()
		transition.duration = 0.5
		transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
		transition.type = CATransitionType.fade
		transition.subtype = CATransitionSubtype.fromBottom
		navigationController?.view.layer.add(transition, forKey: nil)
		_ = navigationController?.popViewController(animated: false)
	}
}
