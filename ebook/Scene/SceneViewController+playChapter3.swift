import UIKit

extension SceneViewController {
	
	func playChapter3(_ scene: Chapter.SceneName) {
		switch scene {
		case .part1:
			PlayerSound.instance.resetCurrentTime()
			PlayerSound.instance.playSound(.เพลงหน้า11)
			imageView
				.setupRx()
				.setupGifImage("เข้าลูปหน้า11")
				.setupGifImage("ลูปหน้า11")
				.play(disposeBag)
			
			twoButton.rx.tap
				.bind { [weak self] _ in
					self?.popup(name: "popupหน้า11")
				}
				.disposed(by: disposeBag)
		case .part2:
			PlayerSound.instance.playSound(.เพลงหน้า11)
			imageView
				.setupRx()
				.setupGifImage("เข้าลูป1หน้า12")
				.setupGifImage("ลูป1หน้า12")
				.play(disposeBag)
		case .part3:
			PlayerSound.instance.playSound(.เพลงหน้า11)
			imageView
				.setupRx()
				.setupGifImage("เข้าลูป2หน้า12")
				.setupGifImage("ลูป2หน้า12")
				.play(disposeBag)
		case .part4:
			PlayerSound.instance.playSound(.เพลงหน้า11)
			imageView
				.setupRx()
				.setupGifImage("เข้าลูป1หน้า13")
				.setupGifImage("ลูป1หน้า13")
				.play(disposeBag)
		case .part5:
			PlayerSound.instance.playSound(.เพลงหน้า11)
			imageView
				.setupRx()
				.setupGifImage("เข้าลูป2หน้า13")
				.setupGifImage("ลูป2หน้า13")
				.play(disposeBag)
		case .part6:
			PlayerSound.instance.playSound(.เพลงหน้า14)
			imageView
				.setupRx()
				.setupGifImage("เข้าลูป1หน้า14")
				.setupGifImage("ลูป1หน้า14")
				.play(disposeBag)
			
			nineAreaButton.rx.tap
				.bind { [weak self] _ in
					self?.popup(name: "popupหน้า14")
				}
				.disposed(by: disposeBag)
		case .part7:
			PlayerSound.instance.playSound(.เพลงหน้า14)
			imageView
				.setupRx()
				.setupGifImage("เข้าลูป2หน้า14")
				.setupGifImage("ลูป2หน้า14")
				.play(disposeBag)
			
			nineAreaButton.rx.tap
				.bind { [weak self] _ in
					self?.popup(name: "popupหน้า14")
				}
				.disposed(by: disposeBag)
			
		case .end:
			closeView()
		default: break
		}
	}
}
