import UIKit
import RxCocoa
import RxSwift
import SwiftyGif

class MenuViewController: UIViewController {
	
	@IBOutlet weak var gotoChater1Button: UIButton!
	@IBOutlet weak var gotoChater2Button: UIButton!
	@IBOutlet weak var gotoChater3Button: UIButton!
	@IBOutlet weak var backgroundImageView: UIImageView!
	
	var disposeBag = DisposeBag()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		gotoChater1Button.rx.tap
			.do(onNext: { _ in
				PlayerSound.instance.playSound(.เสียงปุ่ม)
			})
			.debounce(0.25, scheduler: MainScheduler.asyncInstance)
			.bind { [weak self] _ in
				let storyboard = UIStoryboard(name: "Scene", bundle: Bundle.main)
				let viewController = storyboard.instantiateInitialViewController() as! SceneViewController
				viewController.chapter = .one
				let transition = CATransition()
				transition.duration = 0.5
				transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
				transition.type = CATransitionType.fade
				transition.subtype = CATransitionSubtype.fromBottom
				self?.view.layer.add(transition, forKey: nil)
				self?.navigationController?.view.layer.add(transition, forKey: nil)
				self?.navigationController?.pushViewController(viewController, animated: false)
			}
			.disposed(by: disposeBag)
		
		gotoChater2Button.rx.tap
			.do(onNext: { _ in
				PlayerSound.instance.playSound(.เสียงปุ่ม)
			})
			.debounce(0.25, scheduler: MainScheduler.asyncInstance)
			.bind { [weak self] _ in
				let storyboard = UIStoryboard(name: "Scene", bundle: Bundle.main)
				let viewController = storyboard.instantiateInitialViewController() as! SceneViewController
				viewController.chapter = .two
				let transition = CATransition()
				transition.duration = 0.5
				transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
				transition.type = CATransitionType.fade
				transition.subtype = CATransitionSubtype.fromBottom
				self?.view.layer.add(transition, forKey: nil)
				self?.navigationController?.view.layer.add(transition, forKey: nil)
				self?.navigationController?.pushViewController(viewController, animated: false)
			}
			.disposed(by: disposeBag)
		
		gotoChater3Button.rx.tap
			.do(onNext: { _ in
				PlayerSound.instance.playSound(.เสียงปุ่ม)
			})
			.debounce(0.25, scheduler: MainScheduler.asyncInstance)
			.bind { [weak self] _ in
				let storyboard = UIStoryboard(name: "Scene", bundle: Bundle.main)
				let viewController = storyboard.instantiateInitialViewController() as! SceneViewController
				viewController.chapter = .three
				let transition = CATransition()
				transition.duration = 0.5
				transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
				transition.type = CATransitionType.fade
				transition.subtype = CATransitionSubtype.fromBottom
				self?.view.layer.add(transition, forKey: nil)
				self?.navigationController?.view.layer.add(transition, forKey: nil)
				self?.navigationController?.pushViewController(viewController, animated: false)
			}
			.disposed(by: disposeBag)
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		PlayerSound.instance.stop()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		backgroundImageView.setGifImage(UIImage(gifName: "สารบัญ"))
		PlayerSound.instance.playSound(.เพลงหน้าแรก_สารบัญ, numberOfLoops: -1)
	}
}
