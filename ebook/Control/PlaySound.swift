import AVFoundation

class PlayerSound: NSObject, AVAudioPlayerDelegate {
	
	enum SoundName: String {
		case เสียงปุ่ม
		case เสียงบรรยาดาศ
		case เสียงเปลี่ยนหน้า
		case เสียงบรรยากาศหน้า3
		case เสียงบรรยากาศหน้า4
		case เสียงบรรยากาศหน้า5_1 = "เสียงบรรยากาศหน้า5-1"
		case เสียงบรรยากาศหน้า5_2 = "เสียงบรรยากาศหน้า5-2"
		case เพลงหน้า6
		case เพลงหน้า7
		case เพลงหน้า10
		case เพลงหน้า11
		case เพลงหน้า14
		case moonlight_sonata = "moonlight sonata"
		case เพลงหน้าแรก_สารบัญ = "เพลงหน้าแรก-สารบัญ"
		case not
		
		var fileType: String {
			switch self {
			case .เสียงบรรยาดาศ,
				 .เสียงบรรยากาศหน้า3,
				 .เพลงหน้า6,
				 .เพลงหน้า7,
				 .เพลงหน้า10,
				 .เพลงหน้า11,
				 .เพลงหน้า14,
				 .moonlight_sonata,
				 .เพลงหน้าแรก_สารบัญ:
				return "mp3"
			default:
				return "wav"
			}
		}
		
		var fileTypeReNewSound: Bool {
			return self == .เสียงบรรยาดาศ
				|| self == .เสียงบรรยากาศหน้า3
				|| self == .เสียงบรรยากาศหน้า4
				|| self == .เสียงบรรยากาศหน้า5_1
				|| self == .เสียงบรรยากาศหน้า5_2
				|| self == .moonlight_sonata
		}
	}
	
	static var instance = PlayerSound()
	var players: [URL: AVAudioPlayer] = [:]
	var duplicatePlayers: [AVAudioPlayer] = []
	var player: AVAudioPlayer?
		
	func playSound(_ soundFileName: SoundName, numberOfLoops: Int = 1, startReNew: Bool = false) {
		guard let bundle = Bundle.main.path(forResource: soundFileName.rawValue, ofType: soundFileName.fileType) else { return }
		let soundFileNameURL = URL(fileURLWithPath: bundle)
		
		if let player = players[soundFileNameURL] {
			if !player.isPlaying {
				if startReNew {
					player.currentTime = 0
				}
				player.numberOfLoops = numberOfLoops
				player.play()
			} else {
				do {
					let duplicatePlayer = try AVAudioPlayer(contentsOf: soundFileNameURL)
					duplicatePlayer.delegate = self
					duplicatePlayers.append(duplicatePlayer)
					if startReNew {
						duplicatePlayer.currentTime = 0
					}
					duplicatePlayer.numberOfLoops = numberOfLoops
					duplicatePlayer.play()
				} catch let error {
					print(error.localizedDescription)
				}
			}
		} else {
			do {
				let player = try AVAudioPlayer(contentsOf: soundFileNameURL)
				players[soundFileNameURL] = player
				if startReNew {
					player.currentTime = 0
				}
				player.prepareToPlay()
				player.play()
			} catch let error {
				print(error.localizedDescription)
			}
		}
	}
	
	func stop() {
		for duplicatePlayer in duplicatePlayers {
			duplicatePlayer.stop()
		}
		for player in players {
			player.value.stop()
		}
	}
	
	func setVolume(volume: Float) {
		for duplicatePlayer in duplicatePlayers {
			duplicatePlayer.setVolume(volume, fadeDuration: 0.25)
		}
		for player in players {
			player.value.setVolume(volume, fadeDuration: 0.25)
		}
	}
	
	private func playSounds(soundFileNames: [SoundName]) {
		for soundFileName in soundFileNames {
			playSound(soundFileName)
		}
	}
	
	private func playSounds(soundFileNames: SoundName...) {
		for soundFileName in soundFileNames {
			playSound(soundFileName)
		}
	}
	
	func resetCurrentTime() {
		for element in PlayerSound.instance.players {
			PlayerSound.instance.players[element.key]?.currentTime = 0
		}
	}
	
	func playSounds(soundFileNames: [String], withDelay: Double) { //withDelay is in seconds
		for (index, soundFileName) in soundFileNames.enumerated() {
			let delay = withDelay * Double(index)
			let _ = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(playSoundNotification(_:)), userInfo: ["fileName": soundFileName], repeats: false)
		}
	}
	
	@objc func playSoundNotification(_ notification: NSNotification) {
		if let soundFileName = notification.userInfo?["fileName"] as? SoundName {
			playSound(soundFileName)
		}
	}
	
	func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
		if let index = duplicatePlayers.firstIndex(of: player) {
			duplicatePlayers.remove(at: index)
		}
	}
}
