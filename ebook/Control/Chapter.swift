enum Chapter {
	case one
	case two
	case three
	
	var scenes: [SceneName] {
		switch self {
		case .one:
			return [.part1, .part2, .part3, .part4, .part5, .part6, .part7, .part8, .part9, .part10, .part11, .end]
		case .two:
			return [.part1, .part2, .part3, .part4, .part5, .end]
		case .three:
			return [.part1, .part2, .part3, .part4, .part5, .part6, .part7, .end]
		}
	}
	
	enum SceneName {
		case part1
		case part2
		case part3
		case part4
		case part5
		case part6
		case part7
		case part8
		case part9
		case part10
		case part11
		case end
	}
}

