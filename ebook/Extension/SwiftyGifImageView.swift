import SwiftyGif
import RxSwift
import RxCocoa

class RxSwiftyGifImageView: UIImageView, SwiftyGifDelegate {
	
	enum FlagMode {
		case rx
		case normal
	}
	
	private var onNext: PublishSubject<RxSwiftyGifImageView> = .init()
	private var gifIndex: Int = Int()
	private var listGif = [UIImage]()
	private var flagMode = FlagMode.normal
	
	@discardableResult
	func setupRx() -> Self {
		delegate = self
		return self
	}
	
	func play(_ disposeBag: DisposeBag) {
		setGifImage(listGif.first ?? UIImage())
		onNext
			.bind { [weak self] (imageView) in
				let gif = imageView.listGif[imageView.gifIndex]
				self?.setGifImage(gif)
			}
			.disposed(by: disposeBag)
	}
	
	func setupGifImage(_ gifName: String) -> RxSwiftyGifImageView {
		flagMode = .normal
		let gifImage = UIImage(gifName: gifName)
		loopCount = -1
		gifImage.displayRefreshFactor = 5
		listGif.append(gifImage)
		return self
	}
	
	func rxGifImage(_ gifName: String) -> Observable<RxSwiftyGifImageView> {
		flagMode = .rx
		let gifImage = UIImage(gifName: gifName)
		loopCount = -1
		gifImage.displayRefreshFactor = 5
		setGifImage(gifImage)
		return onNext
	}
	
	func gifDidLoop(sender: UIImageView) {
		if flagMode == .normal {
			guard listGif.count > (gifIndex + 1) else { return }
			gifIndex += 1
			onNext.onNext(self)
		}
		
		if flagMode == .rx {
			onNext.onNext(self)
		}
	}
	
	func dispose() {
		listGif.removeAll()
		gifIndex = 0
		delegate = nil
	}
}

class SwiftyGifImageView: UIImage {
	var name: String?
	
	func reSetGif(_ name: String) {
		self.name = name
		super.setGif(name)
	}
}
