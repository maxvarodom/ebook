import UIKit

class PopupImageView: UIImageView {
	
	func setup() {
		let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
		isUserInteractionEnabled = true
		addGestureRecognizer(tapGestureRecognizer)
	}
	
	@objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
		PlayerSound.instance.playSound(.เสียงปุ่ม)
		removeFromSuperview()
	}
}
