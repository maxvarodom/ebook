import UIKit
import RxSwift
import RxCocoa

class HomeViewController: UIViewController {
	
	@IBOutlet weak var gotoMenuButton: UIButton!
	@IBOutlet weak var backgroundImageVIew: RxSwiftyGifImageView!
	
	var disposeBag = DisposeBag()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		PlayerSound.instance.playSound(.เพลงหน้าแรก_สารบัญ, numberOfLoops: -1)
		backgroundImageVIew.setGifImage(UIImage(gifName: "หน้าแรก2"))
		gotoMenuButton.rx.tap
			.bind { [weak self] _ in
				PlayerSound.instance.playSound(.เสียงปุ่ม)
				let storyboard = UIStoryboard(name: "Menu", bundle: Bundle.main)
				let viewController = storyboard.instantiateInitialViewController() as! MenuViewController
				let transition = CATransition()
				transition.duration = 0.5
				transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
				transition.type = CATransitionType.fade
				transition.subtype = CATransitionSubtype.fromBottom
				self?.view.layer.add(transition, forKey: nil)
				self?.navigationController?.view.layer.add(transition, forKey: nil)
				self?.navigationController?.pushViewController(viewController, animated: false)
			}
			.disposed(by: disposeBag)
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		PlayerSound.instance.stop()
	}
}
